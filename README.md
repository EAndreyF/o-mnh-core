# Development

### Install dependencies

    $ git clone ...
    $ cd intercom

    $ npm install

### Run webpack dev server

    $ npm start

# Production build

    $ npm run build

After build, compiled application will be placed in `dist/` folder.
