angular.module('intercom').config(function($provide) {
    $provide.decorator('taOptions', ['taRegisterTool', '$delegate',
        function(taRegisterTool, taOptions) {

            taOptions.toolbar = [
                ['bold', 'italics', 'h1', 'h2', 'insertLink', 'justifyLeft', 'justifyCenter']
            ];
            taOptions.classes = {
                focussed: 'focussed',
                    toolbar: 'btn-toolbar',
                    toolbarGroup: 'btn-group m-b-sm',
                    toolbarButton: 'btn btn-default btn-sm',
                    toolbarButtonActive: 'active',
                    disabled: 'disabled',
                    textEditor: 'form-control',
                    htmlEditor: 'form-control'
            };

            return taOptions;
        }]);
});
