var datepckerTemplateUrl = require('../templates/datepicker/datepicker.html');
var popupTemplateUrl = require('../templates/datepicker/popup.html');

angular.module('intercom').config(function(uibDatepickerConfig, uibDatepickerPopupConfig) {
    uibDatepickerConfig.showWeeks = false;
    uibDatepickerConfig.templateUrl = datepckerTemplateUrl;

    uibDatepickerPopupConfig.showButtonBar = false;
    uibDatepickerPopupConfig.datepickerPopupTemplateUrl = popupTemplateUrl;
    uibDatepickerPopupConfig.datepickerTemplateUrl = datepckerTemplateUrl;
});
