/**
 * Created by max on 2/7/16.
 */

module.exports = angular.module('intercom.utils.notifier', [
        'ui-notification'
    ])
    .config((NotificationProvider) => {
        NotificationProvider.setOptions({
            delay: 5000,
            startTop: 20,
            startRight: 10,
            verticalSpacing: 20,
            horizontalSpacing: 20,
            positionX: 'center',
            positionY: 'top'
        });
    })
    .factory('notifier', (Notification)=> {
        return {
            success: function (msg, title) {
                Notification.success(this.config(title, msg));
            },

            info: function (msg, title) {
                Notification.info(this.config(title, msg));
            },

            config: function (title, msg, delay) {
                var config = { message: msg, delay: (delay || 5000) };
                if(title) {
                    config.title = title;
                }
                return config;
            }
        }

});