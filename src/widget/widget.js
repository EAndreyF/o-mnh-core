/*eslint-disable */

let $ = require('jquery');
let document = window.document;
let template = require('./widget.handlebars');

$(document).ready(()=> {
    $(template()).appendTo(document.body);

    let launcher = $('#intercom-launcher');
    let messanger = $('#intercom-messenger');

    launcher.on('click', ()=> {
        messanger.show();
        launcher.hide();
    });

    $('.intercom-sheet-header-close-button').on('click', ()=> {
        messanger.hide();
        launcher.show();

        return false;
    });
});
