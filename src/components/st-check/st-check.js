module.exports = angular.module('intercom.components.stCheck', ['smart-table'])
    .directive('stCheck', function() {
        class Ctrl {
            // Injected by link function
            //table;

            // Passed through scope
            //row;

            // Check/uncheck row
            onChange(isChecked) {
                if (isChecked) {
                    if (!_.contains(this.table.checkedRows, this.row)) {
                        this.table.checkedRows.push(this.row);
                    }
                } else {
                    _.pull(this.table.checkedRows, this.row);
                }
            }
        }

        return {
            restrict: 'E',
            require: '^stTable',
            controller: Ctrl,
            controllerAs: 'ctrl',
            bindToController: true,
            scope: {
                row: '='
            },
            template: '<label class="ui-check m-a-0"><input type="checkbox" ng-model="ctrl.isChecked" ng-change="ctrl.onChange(ctrl.isChecked)"><i class="dark-white"></i></label>',
            link: function(scope, element, attrs, table) {
                // Inject table to ctrl
                scope.ctrl.table = table;

                // Initialize checkedRows array on the table controller
                if (!table.checkedRows) table.checkedRows = [];

                // Remove row from checkedRows after scope destroyed
                // Otherwise row will be preserved after pagination
                scope.$on('$destroy', function () {
                    scope.ctrl.onChange(false);
                });

                // In case if checkedRows will be modified somewhere else
                scope.$watchCollection(()=> table.checkedRows, ()=> {
                    if (!_.contains(table.checkedRows, scope.ctrl.row)) {
                        scope.ctrl.isChecked = false;
                    }
                });
            }
        }
    });
