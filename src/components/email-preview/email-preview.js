/**
 * Automation details.
 */

var templateUrl = require('./email-preview.html');

require('./email-preview.scss');

module.exports = angular.module('intercom.components.email-preview', [
    ])

    .directive('emailPreview', function() {
        class Controller {
            constructor() {
            }
        }

        return {
            restrict: 'E',
            templateUrl,
            controller: Controller,
            controllerAs: 'ctrl',
            scope: {
                message: '=',
                sender: '='
            },
            bindToController: true
        };
    });
