/**
 * Company profile page.
 */

var templateUrl = require('./company-profile.html');

module.exports = angular.module('intercom.components.companyProfile', [
    require('../../services/companies-srv.js').name,
    require('../../services/users-srv.js').name,
    require('../../services/conversations-srv.js').name
])

    .directive('companyProfile', function($stateParams, companiesSrv, usersSrv, conversationsSrv) {
        class Controller {
            constructor() {
                this.noteText = '';
                this.company = companiesSrv.getById($stateParams.id);
                this.currentUser = usersSrv.getOne(0);
                this.conversations = conversationsSrv.getAll();
            }

            addNote(text) {
                if (!text) return;

                this.company.notes.unshift({
                    user: usersSrv.getOne(0),
                    text,
                    createdAt: _.now()
                });

                this.noteText = '';
            }
        }

        return {
            restrict: 'E',
            templateUrl,
            controller: Controller,
            controllerAs: 'ctrl',
            scope: {},
            bindToController: true
        };
    });
