/**
 * Expose DOM element to current scope on specified path.
 */

module.exports = angular.module('intercom.components.referElement', [])
    .directive('referElement', function($parse) {
        return {
            restrict: 'A',
            link: function(scope, element) {
                var path = element[0].getAttribute('refer-element');
                $parse(path).assign(scope, element[0]);
            }
        }
    });
