module.exports = {
    enum: {
        PAUSED: 0,
        LIVE: 1,
        DRAFT: 2
    },
    all: [
        {
            name: 'PAUSED',
            className: 'amber'
        },
        {
            name: 'LIVE',
            className: 'green'
        },
        {
            name: 'Draft',
            className: 'blue-grey'
        }
    ]
};