/**
 * Status label.
 */

var templateUrl = require('./status-label.html');
var status = require('../status-label/status.js').all;

module.exports = angular.module('intercom.components.status-label', [])

    .directive('statusLabel', function() {
        class Controller {
            constructor() {
                this.status = status;
            }
        }

        return {
            restrict: 'E',
            templateUrl,
            controller: Controller,
            controllerAs: 'ctrl',
            scope: {
                code: '='
            },
            bindToController: true
        };
    });
