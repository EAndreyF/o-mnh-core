/**
 * Created by max on 2/9/16.
 */

/**
 * Company profile page.
 */

var templateUrl = require('./general-settings.html');
require('./section/general.html');
require('./section/teammates.html');
require('./section/messaging.html');
require('./section/app-messenger.html');


module.exports = angular.module('intercom.components.generalSettings', [
        'ui.select',
        require('../../utils/notifier').name,
        require('../../services/settings-srv.js').name
    ])

    .filter('autocompleteUserFilter', function() {

        function isUserNotMember(userId, members) {
            var result = true;
            _.each(members, (member) => {
                if(member.id === userId) {
                    result = false;
                }
            });
            return result;
        }

        return function(users, props) {
            let out = [];

            if (_.isArray(users)) {
                _.each(users, (user) => {
                    var text = props.name.toLowerCase();
                    if (user.name.toString().toLowerCase().indexOf(text) !== -1) {

                        if(isUserNotMember(user.id, props.members)) {
                            out.push(user);
                        }
                    }
                });
            } else {
                out = users;
            }
            return out;
        }
    })

    .directive('generalSettings', function($state, $stateParams, $document, $log, notifier, settingsSrv) {
        class Controller {
            constructor() {

                this.selectedUser = {};

                this.selectedTeam = {};

                this.settings = settingsSrv.getGeneralSettings();

                this.timeZones = settingsSrv.getTimeZones();

                this.languages = settingsSrv.getLanguages();

                this.emojis = settingsSrv.getEmojis();

                this.sections = [
                    {alias: 'general',      name: 'General', menu: 1, hidden: false },
                    {alias: 'teammates',      name: 'Teammates', menu: 1, hidden: false },
                    {alias: 'messaging', name: 'Messaging', menu: 2, hidden: false },
                    {alias: 'app-messenger', name: 'Application Messenger', menu: 2, hidden: false},
                    {alias: 'appearance', name: 'Appearance', menu: 2, hidden: false},
                    {alias: 'email-templates', name:'Email Templates', menu: 2, hidden: false},
                    {alias: 'email-addresses', name:'Email Addresses', menu: 2, hidden: false},
                    {alias: 'tags', name:'Tags', menu: 3, hidden: false},
                    {alias: 'people-segments', name:'People Segments', menu: 3, hidden: false},
                    {alias: 'company-segments', name:'Company Segments', menu: 3, hidden: false},
                    {alias: 'people-attrs', name:'People Attributes', menu: 3, hidden: false},
                    {alias: 'company-attrs', name:'Company Attributes', menu: 3, hidden: false},
                    {alias: 'events', name:'Event', menu: 3, hidden: false},
                    {alias: 'blocked-people', name:'Blocked people', menu: 3, hidden: false}
                ];

                this.section = _.find(this.sections, {alias: $stateParams.section}) || _.first(this.sections);

                this.loggedUser = _.first(this.settings.teammates.users); //take the first user
            }

            updateAppDetails() {
                notifier.success('Your App settings have been updated');
            }

            deleteApp() {
                notifier.success('Your App was deleted');
            }

            // INVITES MANAGEMENT

            sendInvite() {
                if(this.settings.teammates.invitation) {
                    var user = {
                        email: this.settings.teammates.invitation.email,
                        permissions: {
                            access: this.settings.teammates.invitation.access,
                            exportDenied: this.settings.teammates.invitation.exportDenied,
                            messagingRestricted: this.settings.teammates.invitation.messagingRestricted
                        }
                    };
                    this.settings.teammates.invites.push(user);
                }
                // reset
                this.settings.teammates.invitation = { email: '', access: 'full', exportDenied: false, messagingRestricted: false };
            }

            resendInvite(invite) {
                notifier.info('The invite to '+invite.email+' has been resent');
            }

            revokeInvite(invite) {
                _.remove(this.settings.teammates.invites, { email: invite.email });
            }

            openPermissionsManagement(user) {
                this.selectedUser = angular.copy(user);
            }

            changePermissions() {
                var users = this.selectedUser.draft ? this.settings.teammates.invites : this.settings.teammates.users;
                _.each(users, (user) => {
                   if(user && user.id === this.selectedUser.id) {
                       user.permissions = this.selectedUser.permissions;

                       notifier.info('Permissions for '+(user.name ||  user.email)+' has been changed.');
                   }
                });

                // reset
                this.selectedUser = {};
            }

            // TEAMS MANAGEMENT

            openTeamManagement(team) {
                if(team) {
                    this.selectedTeam = angular.copy(team);
                } else {
                    this.selectedTeam = angular.copy(_.first(this.settings.teammates.teams));
                }

                var ctrl = this;
                angular.element('#teamManagementDialog').on('hide.bs.modal', function(e) {

                    if(ctrl.userTeamForm.$dirty) {
                        e.preventDefault();
                        angular.element('#confirmationDischargeDialog').modal();

                        angular.element('#confirmationDischargeDialog').on('hide.bs.modal', function(e) {

                            if(!ctrl.userTeamForm.$dirty) {
                                angular.element('#teamManagementDialog').modal('hide');
                            }
                        });
                    }
                });
            }

            onSelectMember(team, user) {
                this.userTeamForm.$setDirty();

                // add new team member
                if (this.isUserNotMember(user.id, team.members)) {
                    team.members.push(user);
                }
            }

            removeMember(team, user) {
                this.userTeamForm.$setDirty();

                _.remove(team.members, { id: user.id });
            }

            createTeam() {
                this.userTeamForm.$setDirty();

                this.selectedTeam = { avatar: 'office', name: 'New', members: [] }
            }

            saveTeam() {
                var _team = this.selectedTeam;
                if(_team.id || _team.id > -1) {
                    _.each(this.settings.teammates.teams , (team) => {
                        if(team.id === _team.id) {
                            team.name = _team.name;
                            team.avatar = _team.avatar;
                            this.updateMembers(team, _team.members);
                        }
                    });
                } else {
                    _team.id = this.settings.teammates.teams.length;

                    var team = angular.copy(_team);
                    team.id = _team.id;
                    team.name = _team.name;
                    team.avatar = _team.avatar;
                    this.updateMembers(team, _team.members);
                    this.settings.teammates.teams.push(team);
                }

                this.userTeamForm.$setPristine();
            }

            updateMembers(team, newMembers) {
                team.members = [];
                _.each(newMembers, (newMember) => {
                    _.each(this.settings.teammates.users, (user) => {
                        if(newMember.id === user.id) {
                            team.members.push(user);
                            // update user team
                            if (this.isNotUserTeam(team.id, user.teams)) {
                                if (!user.teams) user.teams = [];

                                user.teams.push(team);
                            }
                        }
                    });
                });

            }

            dischargeTeam() {
                var _team = this.selectedTeam;
                var newTeam = true;
                if(_team.id || _team.id > -1) {
                    _.each(this.settings.teammates.teams , (team) => {
                        if(team.id === _team.id) {
                            this.selectedTeam = angular.copy(team);
                            newTeam = false;
                        }
                    });
                }
                if(newTeam) {
                    this.selectedTeam = angular.copy(_.first(this.settings.teammates.teams));
                }

                this.userTeamForm.$setPristine();
            }

            deleteTeam(teamId) {
                _.remove(this.settings.teammates.teams, { id: teamId });
                _.each(this.settings.teammates.users, (user) => {
                    _.remove(user.teams, { id: teamId });
                });
                // display the next team
                this.selectedTeam = angular.copy(_.first(this.settings.teammates.teams));

                this.userTeamForm.$setPristine();
            }

            // USERS MANAGEMENT

            dropUser(userId) {
                // remove from main user list
                _.remove(this.settings.teammates.users, { id: userId });
                // remove from team list
                _.each(this.settings.teammates.teams, (team) => {
                    _.remove(team.members, { id: userId });
                });
            }

            isUserNotMember(userId, members) {
                var result = true;
                _.each(members, (member) => {
                    if(member.id === userId) {
                        result = false;
                    }
                });
                return result;
            }

            isNotUserTeam(teamId, teams) {
                var result = true;
                _.each(teams, (team) => {
                    if(team.id === teamId) {
                        result = false;
                    }
                });
                return result;
            }

            // MESSAGING SETTINGS
            updateMessagingSettings() {
                notifier.info('Your Messaging settings have been updated');
            }
        }

        return {
            restrict: 'E',
            templateUrl,
            controller: Controller,
            controllerAs: 'ctrl',
            scope: {},
            bindToController: true
        };
    });
