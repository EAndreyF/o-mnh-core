/**
 * Engage page.
 */
var faker = require('faker');
var templateUrl = require('./engage.html');

//require('../../../flatkit/libs/js/echarts/build/dist/echarts-all.js');
//window.theme = require('../../../flatkit/libs/js/echarts/build/dist/theme.js'); // eslint-disable-line
//require('../../../flatkit/libs/js/echarts/build/dist/jquery.echarts.js');

module.exports = angular.module('intercom.components.engage', [
        require('../../services/customers-srv.js').name,
        require('../../services/users-srv.js').name
    ])

    .directive('engage', function ($stateParams, $filter, moment, customersSrv, usersSrv) {
        class Controller {
            constructor() {
                window.t = this;
                this.periods = [
                    {title: 'Last 7 days', days: 7},
                    {title: 'Last 28 days', days: 28},
                    {title: 'Last 90 days', days: 90}
                ];
                this.messageTypes = [
                    'In-app Messages',
                    'Emails'
                ];

                this.period = _.first(this.periods).days.toString();
                this.messageType = _.first(this.messageTypes);

                this.setupMessages();
                this.changePeriod();
            }

            setupMessages() {
                this.messages = [];
                let customers = customersSrv.getAll();
                let users = usersSrv.getAll();
                _.times(20, () => {
                    let customersNumber = faker.random.number({min: 1, max: 5});
                    let opens = faker.random.number({min: 0, max: customersNumber});
                    let clicks = faker.random.number({min: 0, max: opens}); // couldn't be more than opens

                    this.messages.push({
                        title: `${faker.commerce.productName()}`,
                        date: moment().subtract(faker.random.number({min: 1, max: 90}), 'days'),
                        from: _.sample(users),
                        opens: opens,
                        opensP: Math.floor(opens / customersNumber * 100),
                        clicks: clicks,
                        clicksP: Math.floor(clicks / customersNumber * 100),
                        customers: _.sample(customers, customersNumber),
                        type: _.sample(this.messageTypes)
                    })
                });
            }

            changeType() {
                this.messagesByPeriodByType = _.filter(this.messagesByPeriod, (message) => {
                    return message.type === this.messageType;
                });

                var total = {clicks: 0, opens: 0, customers: 0, total: true};
                // use _.each, not Array.forEach, due to speed http://jsperf.com/for-foreach-lodash/3
                _.each(this.messagesByPeriodByType, (message) => {
                    total.clicks += message.clicks;
                    total.opens += message.opens;
                    total.customers += message.customers.length;
                });
                total.clicksP = Math.floor(total.clicks / total.customers * 100) || 0;
                total.opensP = Math.floor(total.opens / total.customers * 100) || 0;
                total.customers = new Array(total.customers);

                this.messagesByPeriodByType.push(total);
            }

            changePeriod() {
                this.messagesByPeriod = _.filter(this.messages, (message) => {
                    return +moment(message.date).add(this.period, 'days') > +moment();
                });

                this.changeType();
                this.groupMessageByCount();
            }

            groupMessageByCount() {
                let cst = [];
                let cstHash = {};
                let numHash = {};
                _.each(this.messagesByPeriod, (message) => {
                    !message.total && _.each(message.customers, (customer) => {
                        if (!cstHash[customer.id]) {
                            cstHash[customer.id] = {
                                cst: customer,
                                value: 0
                            };
                        }
                        cstHash[customer.id].value++;
                    });
                });

                _.each(cstHash, (customer) => {
                    var val = customer.value;
                    if (!numHash[val]) {
                        numHash[val] = {
                            customers: [],
                            count: val
                        };
                        cst.push(numHash[val]);
                    }
                    numHash[val].customers.push(customer.cst);
                });

                this.groupedByCountMessages = cst.sort((a, b) => {
                    return a.count - b.count;
                });
            }
        }

        return {
            restrict: 'E',
            templateUrl,
            controller: Controller,
            controllerAs: 'ctrl',
            scope: {},
            bindToController: true
        };
    });
