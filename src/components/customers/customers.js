/**
 * List of customers.
 */

var templateUrl = require('./customers.html');

module.exports = angular.module('intercom.components.customers', [
    require('../../services/customers-srv.js').name,
    require('../users-filter/users-filter').name,
    require('../users-segments/users-segments').name
])

    .directive('customers', function(customersSrv) {
        class Controller {
            constructor() {
                this.records = customersSrv.getAll();
            }
        }

        return {
            restrict: 'E',
            templateUrl,
            controller: Controller,
            controllerAs: 'ctrl',
            scope: {},
            bindToController: true
        };
    });
