/**
 * Created by max on 2/6/16.
 */

/**
 * Company profile page.
 */

var templateUrl = require('./account-settings.html');
require('./section/account.html');
require('./section/change-password.html');
require('./section/notification.html');
require('./section/conversation.html');
require('./section/tags.html');
require('./section/people.html');
require('./section/company.html');

module.exports = angular.module('intercom.components.accountSettings', [
        require('../../utils/notifier').name,
        require('../../services/settings-srv.js').name
    ])

    .directive('accountSettings', function($state, $stateParams, $document, notifier, settingsSrv) {
        class Controller {
            constructor() {

                this.settings = settingsSrv.getAccountSettings();
                this.showAvatar = true;
                this.selectedUser = this.settings.user;

                this.sections = [
                    {alias: 'account',      name: 'Account', menu: 1, hidden: false },
                    {alias: 'change-password', name: 'Change your password', menu: 1, hidden: true },
                    {alias: 'notification', name: 'Notification', menu: 1, hidden: false},
                    {alias: 'conversation', name: 'Conversation Preferences', menu: 1, hidden: false},
                    {alias: 'tags', name:'Tags', menu: 2, hidden: false},
                    {alias: 'people', name:'People Segments', menu: 2, hidden: false},
                    {alias: 'company', name:'Company Segments', menu: 2, hidden: false}
                ];


                this.section = _.find(this.sections, {alias: $stateParams.section}) || _.first(this.sections);
            }

            updateUserDetails() {
                notifier.info('Your settings were successfully updated.');
            }

            openUploadAvatar() {
                this.showAvatar = true;

                var input = angular.element($document[0].createElement('input'));
                input.attr('type', 'file');
                input.trigger('click');
                return false;
            }

            removeAvatar() {
                this.showAvatar = false;
            }

            openChangePassword() {
                $state.go('account-settings', { section: 'change-password' });
            }

            closeChangePassword() {
                $state.go('account-settings', { section: 'account' });
            }

            updatePassword() {
                notifier.info('Your settings was successfully changed.');
                $state.go('account-settings', { section: 'account' });
            }

            updateNotification() {
                notifier.info('Your settings have been saved.');
            }

            updateConversation() {
                notifier.info('Account settings saved successfully.');
            }

            setupAuth() {
                alert('TBD');
            }

            showAllVisibilities() {
                _.each(this.settings.visibilities, (visibility)=> {
                    visibility.enabled = true;
                });
            }

            hideAllVisibilities() {
                _.each(this.settings.visibilities, (visibility)=> {
                    visibility.enabled = false;
                });
            }

            updateVisibility(visibility) {
                visibility.enabled = !visibility.enabled;
            }

            calcTableHeight() {
                return _.filter(this.settings.visibilities, { type: this.section.alias }).length * 100; // px
            }
        }

        return {
            restrict: 'E',
            templateUrl,
            controller: Controller,
            controllerAs: 'ctrl',
            scope: {},
            bindToController: true
        };
    });
