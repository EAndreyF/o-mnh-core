module.exports = angular.module('intercom.components.stInfiniteScroll', ['smart-table'])
    .directive('stInfiniteScroll', function($timeout, stConfig) {
        return {
            restrict: 'A',
            require: '^stTable',
            link: function(scope, element, attrs, table) {
                var itemByPage = stConfig.pagination.itemsByPage;
                var pagination = table.tableState().pagination;
                var lengthThreshold = 50;
                var timeThreshold = 400;
                var handler = function () {
                    //call next page
                    table.slice(0, pagination.number + itemByPage);
                };
                var promise = null;
                var lastRemaining = 9999;
                /*eslint-disable angular/angularelement */
                var scrollbarElement = $(element).parents(attrs.stInfiniteScroll || 'body');

                function onScroll() {
                    var remaining = scrollbarElement[0].scrollHeight - (scrollbarElement[0].clientHeight + scrollbarElement[0].scrollTop);

                    //if we have reached the threshold and we scroll down
                    if (remaining < lengthThreshold && (remaining - lastRemaining) < 0) {

                        //if there is already a timer running which has no expired yet we have to cancel it and restart the timer
                        if (promise !== null) {
                            $timeout.cancel(promise);
                        }
                        promise = $timeout(function () {
                            handler();

                            //scroll a bit up
                            //container[0].scrollTop -= 500;

                            promise = null;
                        }, timeThreshold);
                    }
                    lastRemaining = remaining;
                }

                // Start pagination
                if (!table.tableState().pagination.number) {
                    table.slice(0, stConfig.pagination.itemsByPage);
                }

                scrollbarElement.bind('scroll', onScroll);
                scope.$on('$destroy', ()=> {
                    scrollbarElement.off('scroll', onScroll);
                });
            }
        }
    });
