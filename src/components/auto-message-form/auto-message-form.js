/**
 * Auto message wizzard.
 */

var templateUrl = require('./auto-message-form.html');
_.times(8, (i)=> {
    require(`./steps/step-${i + 1}.html`);
    require(`./steps/step-${i + 1}-info.html`);
    require(`./steps/step-${i + 1}-additional.html`);
});

//var faker = require('faker');

module.exports = angular.module('intercom.components.autoMessageForm', [
    'textAngular',
    require('../../services/users-srv.js').name,
    require('../../services/customers-srv.js').name,
    require('../../services/email-templates-srv.js').name
])

    .directive('autoMessageForm', function(usersSrv, customersSrv, emailTemplatesSrv) {
        class Controller {
            constructor() {
                this.name = '';

                this.letters = ['A', 'B', 'C', 'D'];

                this.users = usersSrv.getAll();
                this.sender = _.first(this.users);
                this.receiver = _.first(this.users);
                this.customers = customersSrv.getAll();
                this.templates = emailTemplatesSrv.getAll();
                this.template = _.first(this.templates);

                this.nameStep = {
                    title: 'Name',
                    description: 'Name your message',
                    isCompleted: ()=> !!this.name,
                    isVisible: ()=> true
                };

                this.audienceStep = {
                    title: 'Audience',
                    description: 'Choose your audience',
                    isCompleted: ()=> _.any(this.audienceStep.selectedAttributes, (attribute)=> attribute.selected && attribute.predicates.length),
                    selectedAttributes: [],
                    isVisible: ()=> true
                };

                this.channelStep = {
                    title: 'Channel',
                    description: 'Select your channel',
                    isCompleted: ()=> this.isChannelSelectedByUser,
                    onExpand: ()=> this.isChannelSelectedByUser = true,
                    isVisible: ()=> true
                };

                this.messageStep = {
                    title: 'Message',
                    description: 'Write your message',
                    isCompleted: ()=> {
                        return _.every(this.versions, version => {
                            return !!(version.body && version.body.length);
                        });
                    },
                    showAdditional: true,
                    isVisible: ()=> true
                };

                this.deliveryScheduleEnabled = false;
                this.displayWindowStep = {
                    isOptional: true,
                    title: 'Delivery Window',
                    description: 'Set a delivery window',
                    isCompleted: ()=> this.deliveryScheduleEnabled,
                    isVisible: ()=> this.selectedChannel.alias == 'email'
                };
                this.hours = _.times(24, (i) => {
                    let ampm = i < 12 ? 'AM' : 'PM',
                        h = i % 12;
                    return (h === 0 ? '12' : h) + ':00' + ampm;
                });

                this.isStopDateSelectedByUser = false;
                this.stopDate = _.now();
                this.stopDateStep = {
                    isOptional: true,
                    title: 'Stop date',
                    description: 'Set a stop date',
                    isCompleted: ()=> this.isStopDateSelectedByUser,
                    onExpand: ()=> this.isStopDateSelectedByUser = true,
                    isVisible: ()=> true
                };

                this.isGoalSelectedByUser = false;
                this.goalStep = {
                    isOptional: true,
                    title: 'Goal',
                    description: 'Set a goal',
                    isCompleted: ()=> this.isGoalSelectedByUser,
                    onExpand: ()=> this.isGoalSelectedByUser = true,
                    goal: null,
                    isVisible: ()=> true
                };

                this.reviewStep = {
                    title: 'Review',
                    description: 'Review and set it live',
                    isCompleted: ()=> false,
                    isVisible: ()=> true
                };

                this.steps = [
                    this.nameStep,
                    this.audienceStep,
                    this.channelStep,
                    this.messageStep,
                    this.displayWindowStep,
                    this.stopDateStep,
                    this.goalStep,
                    this.reviewStep
                ];

                this.getStepIndex = step => {
                    var index = 0,
                        stepIndex = _.indexOf(this.steps, step);

                    if (stepIndex > -1) {
                        for (let i = 0; i < stepIndex; i++) {
                            let s = this.steps[i];
                            if (s.isVisible()) {
                                index++;
                            }
                        }
                    }

                    return index;
                };

                this.inappTypes = [
                    {name: 'Chat', icon: 'comment-o'},
                    {name: 'Small Announcement', icon: 'bell-o'},
                    {name: 'Big Announcement', icon: 'bullhorn'}
                ];
                this.replyTypes = [
                    {name: 'Text', icon: 'commenting-o'},
                    {name: 'Thumbs', icon: 'thumbs-o-up'},
                    {name: 'Emotions', icon: 'smile-o'}
                ];
                this.channels = [
                    {
                        alias: 'inapp',
                        name: 'In-App Message',
                        title: 'In-app message',
                        icon: 'comment-o',
                        advantages: [
                            'Delivered inside your product',
                            'Up to 10x more engaging than email',
                            'Perfect for reaching active users'
                        ],
                        targetPage: false,
                        type: _.first(this.inappTypes),
                        replyType: _.first(this.replyTypes)
                    },
                    {
                        alias: 'email',
                        name: 'Email',
                        title: 'Email message',
                        icon: 'envelope-o',
                        advantages: [
                            'Delivered to a user\'s email inbox',
                            'Use your own custom email templates',
                            'Ideal for re-engaging inactive users'
                        ],
                        includeUnsubscribe: true
                    }
                ];
                this.selectedChannel = _.first(this.channels);
                this.isChannelSelectedByUser = false;


                this.previewTypes = [
                    {name: 'Desktop', icon: 'desktop'},
                    {name: 'Mobile', icon: 'mobile'}
                ];
                this.previewType = _.first(this.previewTypes);

                this.previewStates = [
                    {name: 'Opened', icon: 'expand'},
                    {name: 'Initial', icon: 'compress'}
                ];
                this.previewState = _.first(this.previewStates);

                this.previewDataTypes = [
                    {name: 'Placeholders', icon: 'ellipsis-h'},
                    {name: 'Sample user', icon: 'user'}
                ];
                this.previewDataType = _.first(this.previewDataTypes);

                this.versions = [
                    {
                        sender: this.sender,
                        receiver: this.receiver,
                        selectedChannel: this.selectedChannel,
                        template: this.template,
                        previewType: this.previewType,
                        previewState: this.previewState,
                        previewDataType: this.previewDataType,
                        body: ''
                    }
                ];

                this.activeVersion = _.first(this.versions);

                this.removeVersion = version => {
                    this.versions = _.without(this.versions, version);
                };

                this.selectChannel = channel => {
                    this.selectedChannel = channel;

                    _.forEach(this.messageVersions, version => {
                        version.selectedChannel = channel;
                    });
                };

                this.startABTest = ()=> {
                    var A = _.first(this.versions),
                        B = _.clone(A);

                    _.extend(B, {
                        $$hashKey: (new Date()).getTime().toString(16),
                        sender: _.clone(A.sender),
                        receiver: _.clone(A.receiver),
                        selectedChannel: _.clone(A.selectedChannel),
                        template: _.clone(A.template),
                        previewType: A.previewType,
                        previewState: A.previewState,
                        previewDataType: A.previewDataType
                    });

                    this.versions.push(B);
                    this.activeVersion = _.first(this.versions);
                };
            }
        }

        return {
            restrict: 'E',
            templateUrl,
            controller: Controller,
            controllerAs: 'ctrl',
            scope: {},
            bindToController: true
        };
    });
