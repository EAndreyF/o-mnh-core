/**
 * Automation details.
 */

var templateUrl = require('./automation-details.html');
var deleteModalTemplateUrl = require('./delete-automation-modal.html');
var faker = require('faker');
var status = require('../status-label/status.js').enum;

require('./automation-details.scss');
require('./statistics.scss');

module.exports = angular.module('intercom.components.automation-details', [
        require('../../services/users-srv.js').name,
        require('../../services/customers-srv.js').name,
        require('../../services/companies-srv.js').name,
        require('../status-label/status-label.js').name,
        require('./stat-value/stat-value.js').name,
        require('./replies/replies.js').name,
        'ui.bootstrap.modal'
    ])

    .directive('automationDetails', function(usersSrv, customersSrv, companiesSrv, $uibModal) {

        class DeleteModalController {
            constructor(letter) {
                this.letter = letter;
            }
        }

        class Controller {
            constructor() {
                let states = [
                    status.LIVE,
                    status.PAUSED
                ];
                let user = _.sample(usersSrv.getAll());
                let company = _.sample(companiesSrv.getAll());
                let subject = `${faker.commerce.productName()}`;

                this.alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P'];
                this.status = status;

                this.record = {
                    title: subject,
                    sender: {
                        user,
                        company
                    },
                    versions: [
                        createVersionMock(),
                        createVersionMock()
                    ]
                };

                this.view = {
                    statExpanded: false,
                    tab: 'message' /** message|replies */
                };

                this.activeVersion = _.first(this.record.versions);

                /** Methods **/

                function createVersionMock() {
                    var sent = _.random(1, 500),
                        replied = _.random(0, Math.round(sent / _.random(1, 10)));

                    return {
                        state: _.sample(states),
                        message: {
                            subject,
                            date: new Date(),
                            text: faker.hacker.phrase()
                        },
                        stop: new Date(Date.now() + _.random(1, 30) * 24 * 3600 * 1000),
                        goals: [
                            {
                                name: 'viewed-visitor-auto-message-list',
                                value: _.random(1, 30)
                            }
                        ],

                        statistics: {
                            sent,
                            opened: _.random(0, sent),
                            goal: _.random(0, sent),
                            clicked: _.random(0, sent),
                            replied,
                            unsubscribed: _.random(0, sent),
                            bounced: _.random(0, sent),
                            links: _.random(0, sent),
                            spam: _.random(0, sent)
                        },

                        replies: _.times(replied, function () {
                            var reply = {
                                customer: _.sample(customersSrv.getAll()),
                                user: _.sample(usersSrv.getAll()),
                                messages: [faker.hacker.phrase()],
                                sent: new Date(Date.now() - _.random(1, 15) * 24 * 3600 * 1000),
                                opened: new Date(Date.now() - _.random(1, 15) * 24 * 3600 * 1000),
                                replied: new Date(Date.now() - _.random(1, 15) * 24 * 3600 * 1000)
                            };

                            return reply;
                        })
                    };
                }

                this.isVersionActive = version => this.activeVersion === version;

                this.setActiveVersion = version => { this.activeVersion = version; };

                this.getRecordStatus = () => {
                    return _.every(this.record.versions, {state: status.PAUSED}) ? status.PAUSED : status.LIVE;
                };

                this.pause = version => { version.state = status.PAUSED; };
                this.live = version => { version.state = status.LIVE; };
                this.winner = version => {
                    _.without(this.record.versions, version)
                        .forEach(item => {
                            item.state = status.PAUSED;
                        });
                };

                this.allVersionsLive = () => _.every(this.record.versions, {state: status.LIVE});

                this.getVersionLetter = version => this.alphabet[this.record.versions.indexOf(version)];

                this.deleteVersion = (version) => {
                    var versionLetter = this.getVersionLetter(version),
                        modalInstance = $uibModal.open({
                            templateUrl: deleteModalTemplateUrl,
                            size: 'md',
                            controller: ['letter', DeleteModalController],
                            controllerAs: 'ctrl',
                            resolve: {
                                letter: _.constant(versionLetter)
                            }
                        });

                    modalInstance.result.then(() => {
                        _.remove(this.record.versions, item => item === version);
                        this.setActiveVersion(_.first(this.record.versions));
                    });
                };
            }
        }

        return {
            restrict: 'E',
            templateUrl,
            controller: Controller,
            controllerAs: 'ctrl',
            scope: {},
            bindToController: true
        };
    });
