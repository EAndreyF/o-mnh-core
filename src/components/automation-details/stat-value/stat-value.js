/**
 * Statistics value.
 */

var templateUrl = require('./stat-value.html');

require('../statistics.scss');

module.exports = angular.module('intercom.components.message-statistics-value', [])

    .directive('statValue', function() {
        class Controller {
            constructor() {
                this.relative = true;

                this.getRelativeValue = function () {
                    return Math.round(this.value / (this.total / 100));
                };
            }
        }

        return {
            restrict: 'E',
            templateUrl,
            controller: Controller,
            controllerAs: 'ctrl',
            scope: {
                total: '=',
                value: '=',
                color: '@'
            },
            bindToController: true
        };
    });
