/**
 * Message replies
 */

var templateUrl = require('./replies.html');

module.exports = angular.module('intercom.components.message-replies', [])

    .directive('messageReplies', function() {
        class Controller {
            constructor() {
                var ctrl = this;

                this.view = {
                    tab: 'replies',
                    selected: []
                };

                _.reduce(this.record.replies, (clicks, reply) => {
                    var min = this.record.statistics.clicked / this.record.replies.length,
                        clicked = Math.floor(_.random(0, min * 2));

                    clicks -= clicked;

                    if (clicks > 0) {
                        reply.clicked = clicked;
                    }

                    return clicks;
                }, this.record.statistics.clicked);

                /** Methods **/

                this.setActiveTab = tabName => { this.view.tab = tabName; };
                this.isActiveTab = tabName => this.view.tab === tabName;

                this.toggleSelect = reply => {
                    var index = _.indexOf(this.view.selected, reply);
                    if (index > -1) {
                        this.view.selected = _.without(this.view.selected, reply);
                    } else {
                        this.view.selected.push(reply);
                    }
                };

                this.toggleSelectAll = () => {
                    if (this.isCheckedAll()) {
                        this.view.selected = [];
                    } else {
                        this.view.selected = [];
                        _.forEach(this.record.replies, reply => {
                            this.view.selected.push(reply);
                        });
                    }
                };

                this.isChecked = reply => _.indexOf(this.view.selected, reply) > -1;
                this.isCheckedAll = () => this.view.selected.length === this.record.replies.length;
                this.isCheckedAny = () => this.view.selected.length > 0;
            }
        }

        return {
            restrict: 'E',
            templateUrl,
            controller: Controller,
            controllerAs: 'ctrl',
            scope: {
                record: '=version'
            },
            bindToController: true
        };
    });
