/**
 * Created by max on 2/21/16.
 */

/**
 * Team management dialog.
 */

var templateUrl = require('./team-management.html');

require('./team-management.scss');

module.exports = angular.module('intercom.components.team-management', [
        'ui.select',
        require('../../utils/notifier').name,
        require('../../services/teams-srv.js').name,
        require('../../services/users-srv.js').name
    ])

    .filter('autocompleteUserFilter', function() {

        function isUserNotMember(userId, members) {
            var result = true;
            _.each(members, (member) => {
                if(member.id === userId) {
                    result = false;
                }
            });
            return result;
        }

        return function(users, props) {
            let out = [];

            if (_.isArray(users)) {
                _.each(users, (user) => {
                    var text = props.name.toLowerCase();
                    if (user.name.toString().toLowerCase().indexOf(text) !== -1) {

                        if(isUserNotMember(user.id, props.members)) {
                            out.push(user);
                        }
                    }
                });
            } else {
                out = users;
            }
            return out;
        }
    })

    .directive('teamManagement', function($state, $stateParams, $document, $log, $timeout, notifier, teamsSrv, usersSrv) {
        class Controller {
            constructor() {

                this.teams = teamsSrv.getAll();

                this.users = usersSrv.getAll();

                this.loggedUser = _.first(this.users); //take the first user

                var ctrl = this;

                $timeout(function () { ctrl.openTeamDetails(); }, 500);
            }

            openTeamDetails(team) {
                if(team) {
                    this.selectedTeam = angular.copy(team);
                } else {
                    this.selectedTeam = angular.copy(_.first(this.teams));
                }

                var ctrl = this;
                angular.element('#teamManagementDialog').on('hide.bs.modal', function(e) {

                    if(ctrl.teamManagementForm.$dirty) {
                        e.preventDefault();
                        angular.element('#confirmationDischargeDialog').modal();

                        angular.element('#confirmationDischargeDialog').on('hide.bs.modal', function(e) {

                            if(!ctrl.teamManagementForm.$dirty) {
                                angular.element('#teamManagementDialog').modal('hide');
                            }
                        });
                    }
                });
            }

            onSelectMember(team, user) {
                this.teamManagementForm.$setDirty();

                // add new team member
                if (this.isUserNotMember(user.id, team.members)) {
                    team.members.push(user);
                }
            }

            removeMember(team, user) {
                this.teamManagementForm.$setDirty();

                _.remove(team.members, { id: user.id });
            }

            createTeam() {
                this.teamManagementForm.$setDirty();

                this.selectedTeam = { avatar: 'office', name: 'New', members: [] }
            }

            saveTeam() {
                var _team = this.selectedTeam;
                if(_team.id || _team.id > -1) {
                    _.each(this.teams , (team) => {
                        if(team.id === _team.id) {
                            team.name = _team.name;
                            team.avatar = _team.avatar;
                            this.updateMembers(team, _team.members);
                        }
                    });
                } else {
                    _team.id = this.teams.length;

                    var team = angular.copy(_team);
                    team.id = _team.id;
                    team.name = _team.name;
                    team.avatar = _team.avatar;
                    this.updateMembers(team, _team.members);
                    this.teams.push(team);
                }

                this.teamManagementForm.$setPristine();
            }

            updateMembers(team, newMembers) {
                team.members = [];
                _.each(newMembers, (newMember) => {
                    _.each(this.users, (user) => {
                        if(newMember.id === user.id) {
                            team.members.push(user);
                            // update user team
                            if (this.isNotUserTeam(team.id, user.teams)) {
                                if (!user.teams) user.teams = [];

                                user.teams.push(team);
                            }
                        }
                    });
                });

            }

            dischargeTeam() {
                var _team = this.selectedTeam;
                var newTeam = true;
                if(_team.id || _team.id > -1) {
                    _.each(this.teams , (team) => {
                        if(team.id === _team.id) {
                            this.selectedTeam = angular.copy(team);
                            newTeam = false;
                        }
                    });
                }
                if(newTeam) {
                    this.selectedTeam = angular.copy(_.first(this.teams));
                }

                this.teamManagementForm.$setPristine();
            }

            deleteTeam(teamId) {
                _.remove(this.teams, { id: teamId });
                _.each(this.users, (user) => {
                    _.remove(user.teams, { id: teamId });
                });
                // display the next team
                this.selectedTeam = angular.copy(_.first(this.teams));

                this.teamManagementForm.$setPristine();
            }

            isUserNotMember(userId, members) {
                var result = true;
                _.each(members, (member) => {
                    if(member.id === userId) {
                        result = false;
                    }
                });
                return result;
            }

            isNotUserTeam(teamId, teams) {
                var result = true;
                _.each(teams, (team) => {
                    if(team.id === teamId) {
                        result = false;
                    }
                });
                return result;
            }

        }

        return {
            restrict: 'E',
            templateUrl,
            controller: Controller,
            controllerAs: 'ctrl',
            scope: { selectedTeamId: '=' },
            bindToController: true
        };
    });
