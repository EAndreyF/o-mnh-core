/**
 * Expose controller of stTable to current scope on specified path.
 */

module.exports = angular.module('intercom.components.stRefer', ['smart-table'])
    .directive('stRefer', function($parse) {
        return {
            restrict: 'A',
            require: '^stTable',
            link: function(scope, element, attrs, tableCtrl) {
                var path = element[0].getAttribute('st-refer');
                $parse(path).assign(scope, tableCtrl);
            }
        }
    });
