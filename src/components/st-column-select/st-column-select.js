let templateUrl = require('./st-column-select.html');
module.exports = angular.module('intercom.components.stPager', ['smart-table'])
    .directive('stColumnSelect', function() {
        return {
            restrict: 'E',
            require: '^stTable',
            templateUrl,
            scope: {},
            controller: angular.noop,
            controllerAs: 'ctrl',
            link: function(scope, element, attrs, table) {
                // Inject table to ctrl
                scope.ctrl.table = table;
            }
        }
    });
