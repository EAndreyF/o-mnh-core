require('jquery');
require('jquery-ui');
require('lodash');
require('angular');
require('angular-animate/angular-animate.js');
require('angular-sanitize/angular-sanitize.js');
require('angular-ui-router');
require('angular-ui-bootstrap/ui-bootstrap-tpls.js');
require('angular-ui-notification');
require('angular-translate');
require('angular-emoji-filter-hd');
require('ui-select');
require('angular-smart-table');
require('imports?define=>false!angular-moment'); // Disable AMD
require('./app.scss');

require('../flatkit/libs/jquery/bootstrap/dist/js/bootstrap.js');

// TextAngular
window.taTools = {}; // eslint-disable-line angular/window-service
window.rangy = require('rangy/lib/rangy-core.js'); // eslint-disable-line angular/window-service
require('rangy/lib/rangy-selectionsaverestore.js');
require('textangular/dist/textAngular-sanitize.js');
require('textangular/dist/textAngularSetup.js');
require('textangular/dist/textAngular.js');

angular.module('intercom', [
    'ngAnimate',
    'ngSanitize',
    'ui.router',
    'ui.bootstrap.buttons',
    'ui.bootstrap.modal',
    'ui.bootstrap.accordion',
    'ui.bootstrap.tooltip',
    'ui.bootstrap.datepicker',
    'ui.bootstrap.dropdown',
    'ui.bootstrap.tpls',
    'angularMoment',
    'smart-table',

    require('./components/st-refer/st-refer').name,
    require('./components/st-check/st-check').name,
    require('./components/st-cell/st-cell').name,
    require('./components/st-column-select/st-column-select').name,
    require('./components/st-infinite-scroll/st-infinite-scroll').name,

    require('./directives/disable-all.js').name,
    require('./directives/no-click-through.js').name,
    require('./directives/ui-jp.js').name,
    require('./filters/dash.js').name,

    require('./components/topmenu/topmenu').name,
    require('./components/customers/customers').name,
    require('./components/customer-profile/customer-profile').name,
    require('./components/companies/companies').name,
    require('./components/company-profile/company-profile').name,
    require('./components/conversations/conversations').name,
    require('./components/automations/automations').name,
    require('./components/auto-message-form/auto-message-form').name,
    require('./components/automation-details/automation-details').name,
    require('./components/email-preview/email-preview').name,
    require('./components/activity/activity').name,
    require('./components/mentions/mentions').name,
    require('./components/reports/reports').name,
    require('./components/engage/engage').name,
    require('./components/account-settings/account-settings').name,
    require('./components/general-settings/general-settings').name

]);

require('./routes.js');
require('./initializers/text-angular.js');
require('./initializers/ui-bootstrap.js');

angular.module('intercom').config(function(stConfig) {
    stConfig.pagination.itemsByPage = 25;
});
