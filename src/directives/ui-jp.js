/*eslint-disable*/
module.exports = angular.module('intercom.directives.uiJp', [])

    .directive('uiJp', function uiJp($timeout) {

        function compile() {
            return function link(scope, elm, attrs) {

                function getOptions() {
                    var linkOptions = [];

                    // If ui-options are passed, merge (or override) them onto global defaults and pass to the jQuery method
                    if (attrs.uiOptions) {
                        linkOptions = eval('[' + attrs.uiOptions + ']');
                    }

                    return linkOptions;
                }

                // Call jQuery method and pass relevant options
                function callPlugin() {
                    $timeout(function() {
                        $(elm)[attrs.uiJp].apply($(elm), getOptions());
                    }, 0, false);
                }

                callPlugin();

                if (attrs.uiRefresh) {
                    attrs.$observe('uiRefresh', ()=> {
                        callPlugin();
                    });
                }
            }
        }

        return {
            restrict: 'A',
            compile: compile
        };
    });


