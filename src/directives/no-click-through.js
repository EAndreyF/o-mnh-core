module.exports = angular.module('intercom.directives.noClickThrough', [])

  .directive('noClickThrough', function() {
    return {
      restrict: 'A',
      link: function(scope, element) {
        element.on('click', (event) => {
          event.stopPropagation();
        });
      }
    };
  });
