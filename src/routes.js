/**
 * Config for the router
 */

angular.module('intercom').config(function($stateProvider, $urlRouterProvider/*, $locationProvider*/) {
    $urlRouterProvider.otherwise('/customers');

    $stateProvider
        .state('home', {
            url: '/'
        })
        .state('customers', {
            url: '/customers?{segmentId:int}',
            template: '<customers></customers>',
            resolve: {
                checkForSegment: function($q, $state, $stateParams, segmentsSrv) {
                    if (!$stateParams.segmentId || !_.find(segmentsSrv.getSegments(), {id: $stateParams.segmentId})) {
                        $stateParams.segmentId = _.find(segmentsSrv.getSegments(), {alias: 'active'}).id;
                    }

                    return $q.when('');
                }
            }
        })
        .state('customer-profile', {
            url: '/customers/{id:int}',
            template: '<customer-profile></customer-profile>'
        })
        .state('companies', {
            url: '/companies?{segmentId:int}',
            template: '<companies></companies>',
            resolve: {
                checkForSegment: function($q, $state, $stateParams, segmentsSrv) {
                    if (!$stateParams.segmentId || !_.find(segmentsSrv.getCompanySegments(), {id: $stateParams.segmentId})) {
                        $stateParams.segmentId = _.find(segmentsSrv.getCompanySegments(), {alias: 'active'}).id;
                    }

                    return $q.when('');
                }
            }
        })
        .state('company-profile', {
            url: '/companies/{id:int}',
            template: '<company-profile></company-profile>'
        })
        .state('conversations', {
            url: '/conversations',
            template: '<conversations></conversations>'
        })
        .state('automations', {
            url: '/automations',
            template: '<automations></automations>'
        })
        .state('new-auto-message', {
            url: '/automations/new',
            template: '<auto-message-form></auto-message-form>'
        })
        .state('automation-details', {
            url: '/automations/details',
            template: '<automation-details></automation-details>'
        })
        .state('activity', {
            url: '/activity',
            template: '<activity></activity>'
        })
        .state('mentions', {
            url: '/mentions',
            template: '<mentions></mentions>'
        })
        .state('reports', {
            url: '/reports?type',
            template: '<reports></reports>'
        })
        .state('engage', {
            url: '/engage',
            template: '<engage></engage>'
        })
        .state('account-settings', {
            url: '/account-settings?section',
            template: '<account-settings></account-settings>'
        })
        .state('general-settings', {
            url: '/general-settings?section',
            template: '<general-settings></general-settings>'
        })
    ;

     //$locationProvider.html5Mode(true);
});
