/**
 * Created by max on 2/21/16.
 */

var faker = require('faker');

module.exports = angular.module('intercom.services.teams-srv', [
        require('./users-srv.js').name
])
    .service('teamsSrv', (usersSrv)=> {
        let teams = [];

        let users = usersSrv.getAll();

        var emojies = getEmojis();

        _.times(5, (i)=> {

            //generate teams
            let team = {
                id: i,
                avatar: emojies[faker.random.number(emojies.length-1)],
                icon: faker.image.abstract(),
                name: faker.commerce.department()
            };
            addMembers(team, users);
            teams.push(team);

        });

        function pickUpSome(list) {
            var result = [];

            _.times(faker.random.number(5 < list.length ? 5 : list.length-1), () => {
                var index = faker.random.number(list.length-1);
                result.push(list[index]);
            });

            return _.uniq(result);
        }

        function addMembers(team, list) {
            if(!team.members) team.members = [];

            var users = pickUpSome(list);
            _.each(users, (user) => {
                if(user) {
                    if(!user.teams) user.teams = [];

                    team.members.push(user);
                    user.teams.push(team);
                }
            });
        }

        function getEmojis() {
            var emojis = ['dog', 'cat', 'railway_car', 'smile', 'confused', 'angry', 'house', 'school', 'hotel'];
            return emojis;
        }

        class TeamSrv {

            getCount() {
                return teams.length;
            }

            getAll() {
                return teams;
            }

            getOne(index) {
                return teams[index];
            }

            getEmojis() {
                return getEmojis();
            }
        }

        return new TeamSrv();
    });
