var faker = require('faker');

module.exports = angular.module('intercom.services.usersSrv', [])
    .service('usersSrv', ()=> {
        let users = [];

        _.times(9, (i)=> {
            users.push({
                id: i,
                name: i ? faker.name.findName() : 'John Smith',
                email: faker.internet.email(),
                avatar: require(`../../flatkit/assets/images/a${i % 9}.jpg`)
            });
        });

        class UsersSrv {
            getAll() {
                return users;
            }

            getOne(index) {
                return users[index];
            }
        }

        return new UsersSrv();
    });
