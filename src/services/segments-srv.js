module.exports = angular.module('intercom.services.segmentsSrv', [])
    .service('segmentsSrv', ()=> {
        let segments = [
            {
                id: 1,
                name: 'All Users',
                count: 186,
                isPredefined: true,
                isEditable: false,
                icon: 'users'
            },
            {
                id: 2,
                name: 'All Leads',
                count: 0,
                isPredefined: true,
                isEditable: false,
                icon: 'users'
            },
            {
                id: 3,
                name: 'New',
                count: 10,
                isPredefined: true,
                isEditable: true,
                predicates: [
                    {type: 'date', attribute: 'signed_up', comparison: 'less_than', value: 3}
                ],
                icon: 'user'
            },
            {
                id: 4,
                name: 'Active',
                alias: 'active',
                count: 95,
                isPredefined: true,
                isEditable: true,
                predicates: [
                    {type: 'date', attribute: 'last_request_at', comparison: 'less_than', value: 30}
                ],
                icon: 'line-chart'
            },
            {
                id: 5,
                name: 'Slipping Away',
                count: 0,
                isPredefined: true,
                isEditable: true,
                predicates: [
                    {type: 'date', attribute: 'last_request_at', comparison: 'more_than', value: 30},
                    {type: 'date', attribute: 'last_request_at', comparison: 'less_than', value: 60}
                ],
                icon: 'level-down'
            },
            {
                id: 6,
                name: 'My custom segment',
                count: 7,
                isPredefined: false,
                isEditable: true,
                predicates: [
                    {type: 'string', attribute: 'name', comparison: 'ends', value: 'skiy'}
                ],
                icon: 'pie-chart'
            }
            //,
            //{
            //    id: 7,
            //    name: 'Another custom segment',
            //    count: 42,
            //    isPredefined: false,
            //    isEditable: true,
            //    predicates: [
            //        {type: 'string', attribute: 'email', comparison: 'ends', value: '@gmail.com'},
            //        {type: 'string', attribute: 'email', comparison: 'ends', value: '@google.com'}
            //    ],
            //    icon: 'pie-chart'
            //}
        ];

        let companySegments = [
            {
                id: 1,
                name: 'All',
                count: 18,
                isPredefined: true,
                isEditable: false,
                icon: 'building'
            },
            {
                id: 2,
                name: 'New',
                count: 10,
                isPredefined: true,
                isEditable: true,
                predicates: [
                    {type: 'date', attribute: 'created_at', comparison: 'less_than', value: 3}
                ],
                icon: 'building-o'
            },
            {
                id: 3,
                name: 'Active',
                alias: 'active',
                count: 18,
                isPredefined: true,
                isEditable: true,
                predicates: [
                    {type: 'date', attribute: 'last_request_at', comparison: 'less_than', value: 30}
                ],
                icon: 'line-chart'
            },
            {
                id: 4,
                name: 'Slipping Away',
                count: 0,
                isPredefined: true,
                isEditable: true,
                predicates: [
                    {type: 'date', attribute: 'last_request_at', comparison: 'more_than', value: 30},
                    {type: 'date', attribute: 'last_request_at', comparison: 'less_than', value: 60}
                ],
                icon: 'level-down'
            }
        ];

        class SegmentsSrv {
            getSegments() {
                return segments;
            }

            createSegment(segment) {
                segments.push(segment);
            }

            getCompanySegments() {
                return companySegments;
            }
        }

        return new SegmentsSrv();
    });
