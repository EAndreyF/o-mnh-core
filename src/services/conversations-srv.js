var faker = require('faker');

module.exports = angular.module('intercom.services.conversationsSrv', [
    require('./users-srv.js').name,
    require('./customers-srv.js').name
])
    .service('conversationsSrv', (usersSrv, customersSrv)=> {
        var conversations = _(9).times((i)=> {
            let user = usersSrv.getOne((i + 1) % 9);
            let customer = customersSrv.getOne(i);

            let conversation = {
                customer,
                user,
                messages: []
            };

            _.times(20, (i)=> {
                let message = {
                    text: faker.hacker.phrase(),
                    createdAt: faker.date.recent()
                };

                if (i % 2) {
                    message.user = user;
                } else {
                    message.customer = customer;
                }

                conversation.messages.push(message);
            });
            conversation.messages = _(conversation.messages).sortBy('createdAt').value();
            conversation.lastMessage = _(conversation.messages).filter('customer').last();

            return conversation;

        }).sortBy('lastMessage.createdAt').reverse().value();

        class ConversationsSrv {
            getAll() {
                return conversations;
            }

            getOne(index) {
                return conversations[index];
            }

            getById(id) {
                return _.find(conversations, {id});
            }
        }

        return new ConversationsSrv();
    });
