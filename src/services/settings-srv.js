/**
 * Created by max on 2/6/16.
 */

var faker = require('faker');

var moment = require('moment-timezone/builds/moment-timezone-with-data');

module.exports = angular.module('intercom.services.settingsSrv', [
        'pascalprecht.translate',
        require('./users-srv.js').name,
        require('./customers-srv.js').name,
        require('./companies-srv.js').name,
        require('./conversations-srv.js').name
    ])
    .service('settingsSrv', (usersSrv, customersSrv, companiesSrv, conversationsSrv) => {

        let now = new Date();
        let twoDaysAgo = new Date(); twoDaysAgo.setDate(now.getDate() - 2);
        let weekAgo = new Date(); weekAgo.setDate(now.getDate() - 7);

        let users = usersSrv.getAll();
        let customers = customersSrv.getAll();
        let companies = companiesSrv.getAll();
        let conversations = conversationsSrv.getAll();

        let accountSettings = {
            user: angular.copy(usersSrv.getOne(0)),
            company: angular.copy(companiesSrv.getByIndex(0)),
            password: {
                current: '',
                create: '',
                confirm: ''
            },
            notification: {
                assignedToMe: { desktop: false, email: true },
                assignedToTeam: { email: true },
                unassigned: { email: false },
                assignedToOthers: { email: true },
                mentionedMe: { email: true },
                unassignedAlerts: { alert: true },
                browserAlerts: { sound: true },
                dailySingnups: { email: true, type: 'users' }
            },
            conversation: {
                assignmentSettings: { type: 'assignToMe' }
            },
            visibilities: []
        };

        // users
        accountSettings.visibilities.push({
            readonly: true,
            default: true,
            type: 'people',
            name: 'All Users',
            creator: { name: 'Default Segment'},
            created: false,
            people: users
        });

        accountSettings.visibilities.push({
            readonly: true,
            default: true,
            type: 'people',
            name: 'All Leads',
            creator: { name: 'Default Segment'},
            created: false,
            people: pickUpSome(users)
        });

        accountSettings.visibilities.push({
            default: true,
            type: 'people',
            name: 'New',
            creator: { name: 'Default Segment'},
            created: false,
            people: pickUpSome(users)
        });

        accountSettings.visibilities.push({
            default: true,
            type: 'people',
            name: 'Active',
            creator: { name: 'Default Segment'},
            created: false,
            people: pickUpSome(users)
        });

        accountSettings.visibilities.push({
            default: true,
            type: 'people',
            name: 'Slipping Away',
            creator: { name: 'Default Segment'},
            created: false,
            people: []
        });

        // companies
        accountSettings.visibilities.push({
            readonly: true,
            default: true,
            type: 'company',
            name: 'All',
            creator: { name: 'Default Segment'},
            created: false,
            companies: companies
        });

        accountSettings.visibilities.push({
            default: true,
            type: 'company',
            name: 'New',
            creator: { name: 'Default Segment'},
            created: false,
            companies: pickUpSome(companies)
        });

        accountSettings.visibilities.push({
            default: true,
            type: 'company',
            name: 'Active',
            creator: { name: 'Default Segment'},
            created: false,
            companies: pickUpSome(companies)
        });

        accountSettings.visibilities.push({
            default: true,
            type: 'company',
            name: 'Slipping Away',
            creator: { name: 'Default Segment'},
            created: false,
            companies: []
        });

        _.times(users.length, (i) => {
            accountSettings.visibilities.push({
                type: 'tags',
                name: faker.commerce.productAdjective(),
                enabled: faker.random.boolean(),
                creator: users[i],
                created: faker.date.recent(),
                people: pickUpSome(customers),
                companies: pickUpSome(companies),
                conversations: pickUpSome(conversations)
            });
            accountSettings.visibilities.push({
                type: 'people',
                name: faker.company.companyName(),
                enabled: faker.random.boolean(),
                creator: users[i],
                created: faker.date.recent(),
                people: pickUpSome(users)
            });
            accountSettings.visibilities.push({
                type: 'company',
                name: faker.company.companyName(),
                enabled: faker.random.boolean(),
                creator: users[i],
                created: faker.date.recent(),
                companies: pickUpSome(companies)
            });

        });

        initSettings(users);

        // Auto generate fake data
        var teams = [], invites = [];
        var emojies = getEmojis();
        _.times(4, (i)=> {

            //generate invites
            let invite = {
                id: i,
                email: faker.internet.email(),
                draft: true
            };
            invites.push(invite);

            //generate teams
            let team = {
                id: i,
                avatar: emojies[faker.random.number(emojies.length-1)],
                icon: faker.image.abstract(),
                name: faker.commerce.department()
            };
            addMembers(team, users);
            teams.push(team);

        });

        initSettings(invites);

        let generalSettings = {
            app: {
                name: 'Stroman - Lubowitz',
                timeZone: 'EST',
                socialData: true,
                companies: true,
                testVersion: true
            },
            teammates: {
                invitation: { email: '', access: 'full', exportDenied: false, messagingRestricted: false },
                invites: invites,
                teams: teams,
                users: users
            },
            messaging: {
                template: 'Hi {{first_name | fallback: "there" }},',
                recipient: {},
                internalEmail: 'internal@mail.com',
                unsubscribed: false
            }
        };

        function pickUpSome(list) {
            var result = [];

            _.times(faker.random.number(5 < list.length ? 5 : list.length-1), () => {
                var index = faker.random.number(list.length-1);
                result.push(list[index]);
            });

            return _.uniq(result);
        }

        function addMembers(team, list) {
            if(!team.members) team.members = [];

            var users = pickUpSome(list);
            _.each(users, (user) => {
                if(user) {
                    if(!user.teams) user.teams = [];

                    team.members.push(user);
                    user.teams.push(team);
                }
            });
        }

        function initSettings(list) {
            _.each(list, (item) => {
                item.permissions = {
                    access: (faker.random.boolean() ? 'full': 'restricted'),
                    exportDenied: faker.random.boolean(),
                    messagingRestricted: faker.random.boolean()
                };
            });
        }

        function getEmojis() {
            var emojis = ['dog', 'cat', 'railway_car', 'smile', 'confused', 'angry', 'house', 'school', 'hotel'];
            return emojis;
        }

        class SettingsSrv {

            getAccountSettings() {
                return accountSettings;
            }

            getGeneralSettings() {
                return generalSettings;
            }

            getTimeZones() {
               return _.sortBy(_.map(moment.tz.names(), function(zoneName) {
                    return {
                        name: zoneName,
                        offset: 'UTC' + moment().tz(zoneName).format('Z')
                    };
                }), 'offset')
            }

            getLanguages() {

            }

            getEmojis() {
                var emojis = ['dog', 'cat', 'railway_car', 'smile', 'confused', 'angry', 'house', 'school', 'hotel'];
                return emojis;
            }
        }

        return new SettingsSrv();
    });
