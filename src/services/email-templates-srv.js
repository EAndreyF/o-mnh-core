module.exports = angular.module('intercom.services.emailTemplatesSrv', [])
    .service('emailTemplatesSrv', ()=> {

        var templates = [
            {
                name: 'Plain',
                preview: require(`../../flatkit/assets/images/c6.jpg`)
            },
            {
                name: 'Personal',
                preview: require(`../../flatkit/assets/images/c1.jpg`)
            },
            {
                name: 'Company',
                preview: require(`../../flatkit/assets/images/c2.jpg`)
            },
            {
                name: 'Announcement',
                preview: require(`../../flatkit/assets/images/c3.jpg`)
            }
        ];

        class EmailTemplatesSrv {
            getAll() {
                return templates;
            }
        }

        return new EmailTemplatesSrv();
    });
