module.exports = angular.module('intercom.filters.dash', [])
  .filter('dash', function() {
    return function(input) {
      return input ? input : '―';
    };
  });
